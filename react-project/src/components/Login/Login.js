import {useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AppContainer from "../hoc/AppContainer"
import { loginAttemptAction } from '../../store/actions/loginActions'

import {NavLink}  from 'react-router-dom'
// hooks work in functional components, not class components

function Login(){

    const dispatch = useDispatch()
    const { loginError, loginAttempting} = useSelector(state => state.loginReducer )
    const { loggedIn} = useSelector(state => state.sessionReducer )

const [ credentials, setCredentials ] = useState({
    username: '',
    password: ''
})
const onInputChange = event => {
    setCredentials({
        ...credentials,
        [event.target.id]: event.target.value
    })
}
const onFormSubmit = event => {
    event.preventDefault()
    dispatch(loginAttemptAction(credentials))
    
}

    return(

        <>
            { loggedIn && <NavLink to="/timeline">Timeline</NavLink> }
            { !loggedIn && 
                <AppContainer>
                    <form className="mt-3" onSubmit={ onFormSubmit }>
                        <h1>Login to TXT forum</h1>
                        <p>Welcome to the forum where text is King! 👑</p>

                        <div className="mb-3">
                            <label htmlFor="username" className="form-label">username
                            </label>
                            {/* onInputChange -> get vari like id etc, same name in function */}
                            <input id="username" type="text" placeholder="Enter your username" 
                            className="form-control" onChange={ onInputChange} />
                            
                        </div>
                        <div className="mb-3">
                            <label htmlFor="password" className="form-label">username
                            </label>
                            <input id="password" type="password" placeholder="Enter your password" 
                            className="form-control" className="form-control" onChange={ onInputChange}></input>
                            
                        </div>
                        <button type="submit" className="btn btn-primary  btn-lg">Login</button>

                    
                    </form>
                    { loginAttempting &&
                        <p>Trying to login...</p>
                    }
                    { loginError &&
                        <div className="alert alert-danger" role="alert">
                            <h4>Unsuccessful</h4>
                            <p className="mb-0">{ loginError }</p>
                        </div>
                    }
                </AppContainer>
            }
            
        </>

        
    )
}

export default Login