import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
// Since we are using index as file name we don't need to write "./reducers/index";
import appReducer from "./reducers";
import middleware from "./middleware";

export default createStore(
    appReducer,
    composeWithDevTools(middleware)
)