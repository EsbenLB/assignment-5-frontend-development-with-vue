const AppContainer = props => {
    return (
        <div className="container">{props.children}</div>
    )
} 
export default AppContainer

// items becomes  children
// {/* <AppContainer>
//     <h1>hello world</h1>
// </AppContainer> */}