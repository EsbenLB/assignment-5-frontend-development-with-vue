import './App.css';
import { BrowserRouter, Routes , Route, NavLink } from 'react-router-dom'
import AppContainer from './components/hoc/AppContainer';
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound'
import Timeline from './components/Timeline/Timeline';

function App() {
  return (
    <BrowserRouter>
      <div className="App" >
        <AppContainer>
          <h1>React Txt Forum</h1>
          <h1>
            <span className="material-icons">fingerprint</span>
          </h1>
        </AppContainer>
        <Routes >
          {/* is exact keyword needed? */}
          <Route path="/" exact element={ <Login/>} />
          <Route path="/timeline" element={ <Timeline/>} />
          <Route path="*" element={ <NotFound/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;